
Shoes.app do
  fill(yellow)
  @cerchio = oval(left: 40, top: 40, radius: 40)
  animate do |i|
    @cerchio.top += (-20..20).rand
    @cerchio.left += (-20..20).rand
  end
end
