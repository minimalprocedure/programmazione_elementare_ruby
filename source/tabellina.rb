
numeri = [1,2,3,4,5,6,7,8,9,10]

for numero in numeri
  riga = ''
  for moltiplicatore in numeri
    valore = numero * moltiplicatore
    if valore < 10
      separatore = '  '
    else
      separatore = ' '
    end
    riga = riga + valore.to_s + separatore
  end
  puts riga
end
