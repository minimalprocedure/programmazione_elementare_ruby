
Shoes.app {
  stack(margin: 6) {
    title("Formattazione del testo")
    para(strong("Domanda"), " Quanto sei alto?")
    para(em(strong("Risposta"), " Sono alto un metro e mezzo."))
  }
}
