
# stampa un array bidimensionale
def stampa_tabella(tabella)
  puts (tabella.map { |riga| 
    (riga.map {|numero| 
      numero.to_s + (numero < 10 ? '  ' : ' ')
    }).join
  }).join("\n")
end

stampa_tabella([[1,10,30,4,50,6,7,8,9,0,9,0],[2,4,6,81,17,12,14,1,1,2], [3,6,9,12,15,18,21,24,27,30]])
