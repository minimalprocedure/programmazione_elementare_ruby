
require 'json'

rubrica = [

  {:nome => 'Massimo', :cognome => 'Ghisalberti', :telefono => '1234567890'},
  {:nome => 'Mario', :cognome => 'Rossi', :telefono => '1234567890'}

]

puts "La rubrica come array"
p rubrica
puts

puts "la rubrica serializzata come YAML"
p rubrica.to_json
puts

puts "Salvo la rubrica su un file"
puts
File.open('database.json', 'w') do |file|
  file.write(rubrica.to_json)
end


puts "Leggo la rubrica su un file"
puts
dati = []
File.open('database.json', 'r') do |file|
  json = JSON.load(file.read)
  dati = json.to_a
end

puts "La rubrica ricaricata dal file"
p dati
