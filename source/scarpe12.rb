
Shoes.app(title: "la tabellina", width: 340, height: 280) {

def tabellina
  numeri = [1,2,3,4,5,6,7,8,9,10]
  (numeri.map { |numero|
          (numeri.map { |moltiplicatore|
             valore = numero * moltiplicatore
            (valore < 10 ? '0' : '') + valore.to_s + ' | '
          }).join
        }).join("\n")
 end

  stack(margin: 10) {
    @tabellina = para(strong(tabellina))
    @tabellina.style(fill: red, stroke: white)
  }
  
}
