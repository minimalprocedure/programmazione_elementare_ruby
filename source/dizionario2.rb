
require 'yaml'

rubrica = [

  {:nome => 'Massimo', :cognome => 'Ghisalberti', :telefono => '1234567890'},
  {:nome => 'Mario', :cognome => 'Rossi', :telefono => '1234567890'}

]

puts "La rubrica come array"
p rubrica
puts

puts "la rubrica serializzata come YAML"
p rubrica.to_yaml
puts

puts "Salvo la rubrica su un file"
puts
File.open('rubrica.yml', 'w') do |file|
  file.write(rubrica.to_yaml)
end


puts "Leggo la rubrica su un file"
puts
dati = []
File.open('rubrica.yml', 'r') do |file|
  yaml = YAML.load(file.read)
  dati = yaml.to_a
end

puts "La rubrica ricaricata dal file"
p dati
