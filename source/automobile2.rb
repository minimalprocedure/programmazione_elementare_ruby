
class Veicolo
end

class VeicoloAMotore < Veicolo

  @@motore = true

  def motore
    @@motore
  end
  
end

class VeicoloARuoteConMotore < VeicoloAMotore

  def initialize(numero_ruote = 4)
    @@ruote = numero_ruote
  end

  def ruote
    @@ruote
  end

end

class Automobile < VeicoloARuoteConMotore

  attr_accessor :persone
  
  def initialize
    super(4)
    @persone = 0
  end

end

Auto = Automobile.new

Auto.persone = 5

puts "Auto ha il motore: " + (Auto.motore ? "si" : "no")
puts "Ospita quante persone? " + Auto.persone.to_s
puts "Quante ruote? " + Auto.ruote.to_s
puts
puts "Variabili di istanza sono:"
puts Auto.instance_variables
