
numeri = [1,2,3,4,5,6,7,8,9,10]
 
puts (numeri.map { |numero|
        (numeri.map { |moltiplicatore|
          valore = numero * moltiplicatore
          separatore = valore < 10 ? '  ' : ' '
          valore.to_s + separatore
        }).join
      }).join("\n")
