
# prepara una tabella (array bidimensionale)
# con i valori
def tabella_pitagorica(numeri)
  numeri.map { |numero|
    numeri.map { |moltiplicatore| 
      numero * moltiplicatore
    }
  }
end

# stampa un array bidimensionale
def stampa_tabella(tabella)
  puts (tabella.map { |riga| 
    (riga.map {|numero| 
      numero.to_s + (numero < 10 ? '  ' : ' ')
    }).join
  }).join("\n")
end

stampa_tabella(tabella_pitagorica([1,2,3,4,5,6,7,8,9,10]))
