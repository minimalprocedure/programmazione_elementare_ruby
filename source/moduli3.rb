
module Pippo

  def dice(cosa)
    puts "Pippo dice: " + cosa
  end

end

module Pluto

  def dice(cosa)
    puts "Pluto dice: " + cosa
  end

end

include Pippo
include Pluto

Pippo.dice("ciao")
Pippo::dice("ciao")
