
Shoes.app(title: "Il bottone") {

  stack {
    @bottone = button( "Premi") 
    @note = para(em("Non hai ancora premuto"))
  }

  num = 0
  
  @bottone.click {
    num += 1
    @note.text = "Hai premuto " + num.to_s + " volte"
  }

}
