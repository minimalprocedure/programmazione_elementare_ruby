
Shoes.app(title: "la tabellina", width: 412, height: 412, resizable: true) {
  
  def tabellina
    numeri = [1,2,3,4,5,6,7,8,9,10]
    numeri.map { |numero|
      numeri.map { |moltiplicatore|
        numero * moltiplicatore
      }
    }
  end
  
  width = 40
  height = 40
  top = 0
  
  tabellina.each_index { |numero_riga|
    stack(margin_left: 2) { 
      flow {
        left = 0        
        tabellina[numero_riga].each_index { |numero_colonna|
          valore =  tabellina[numero_riga][numero_colonna]
          pad = if valore < 10
                  '  '
                elsif valore >= 10 && valore < 100
                  ' '
                else
                  ''
                end          
          button = button(valore.to_s + ' ')
          button.style(top: top, left: left, width: width, height: height)
          button.instance_variable_set(:@coord, [numero_riga + 1, numero_colonna + 1])
          button.click { |btn|
            coord = btn.instance_variable_get(:@coord)      
            alert("Si ottiene moltiplicando #{ coord.join(' x ')}, oppure #{ coord.reverse.join(' x ')}")
          }
          
          left += width + 1
        }
      }
      top += height + 1
    }
  }
}
