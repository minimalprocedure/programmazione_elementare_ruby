
numeri = [1,2,3,4,5,6,7,8,9,10]
 
tabella = numeri.map do |numero|
  riga = numeri.map do |moltiplicatore|
    valore = numero * moltiplicatore
    separatore = valore < 10 ? '  ' : ' '
    valore.to_s + separatore
  end
  riga.join
end
puts tabella.join("\n")
