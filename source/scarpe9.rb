
Shoes.app do

  background "#00ffff"
  border("#ff0000", strokewidth: 6)

  stack(margin: 12) do
    title("Giochino")
    para("Inserisci il tuo nome:")
    flow do
      @input_field = edit_line
      button("OK") do
         alert("Ciao " + @input_field.text)
      end
    end
  end
end
