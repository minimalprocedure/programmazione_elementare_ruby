
class Veicolo
end

class VeicoloAMotore < Veicolo

  attr_reader :motore

  def initialize
    @motore = true
  end
  
end

class VeicoloARuoteConMotore < VeicoloAMotore

  attr_reader :ruote
  
  def initialize(numero_ruote = 4)
    super()
    @ruote = numero_ruote
  end

end

class Automobile < VeicoloARuoteConMotore

  attr_accessor :persone
  
  def initialize
    super(4)
    @persone = 0
  end

end

Auto = Automobile.new

Auto.persone = 5

puts "Auto ha il motore: " + (Auto.motore ? "si" : "no")
puts "Ospita quante persone? " + Auto.persone.to_s
puts "Quante ruote? " + Auto.ruote.to_s
puts
puts "Variabili di istanza sono:"
puts Auto.instance_variables
